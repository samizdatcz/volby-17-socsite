title: "ČSSD píše o mzdách, migraci se věnují jen komunisti a SPD.<br>Stranický Facebook ukazuje priority partají"
perex: "Politické strany na Facebooku žijí okamžikem. Asi polovinu všech stranických příspěvků na síti tvoří čerstvé momentky z kontaktní kampaně. Ze zbytku ale vystupují hlavní témata, se kterými jdou strany do voleb. Ukázala to analýza výzkumníků z Masarykovy univerzity."
authors: ["Jan Cibulka", "Kristýna Novotná"]
published: "19. října 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/kampane_170728-234738_rez.jpg
coverimg_note: "Foto Kristýna Novotná"
styles: []
libraries: ["https://code.highcharts.com/highcharts.js", "https://code.highcharts.com/modules/series-label.js", "https://interaktivni.rozhlas.cz/data/volby-17-socsite/charts/charts.js"]
options: "" #wide
---

Kontaktní kampani se nejvíce věnuje hnutí ANO - v celkem 59 % příspěvků. U něj i u ostatních stran ale platí, že zbylá komunikace odráží partajní priority. Například sociální demokraté věnují 14 % svých statusů mzdovým podmínkám v Česku, zatímco u ostatních stran téma tvoří maximálně jedno procento facebookové komunikace. U ČSSD je výrazné i zaměření na sociální politiku či nezaměstnanost, i když tady jsou si partaje o poznání blíže.

Dalším výrazným programovým zaměřením je otázka migrace. Jak klesá zájem veřejnosti a médií o uprchlickou problematiku, mizí téma i z komunikace stran. Klíčové však zůstává pro SPD Tomia Okamury, v o něco menší míře pak i u komunistů.

_Jaká témata strany komunikují na svých facebookových profilech, můžete prozkoumat v následující grafice. Ukazuje, kolik procent příspěvků strana věnovala danému tématu._

<wide>
<div id="temata_strany" style="min-width: 310px; max-width: 1000px; height: 800px; max-width: 1000px; margin: 0 auto"></div>
</wide>

Pokud jde o negativní kampaň a vymezení se proti ostatním, se skoro 10 procenty vedou Piráti, v závěsu za nimi jsou ale i komunisti, SPD a ODS.

Místopředseda Pirátů a lídr strany na Pardubicku Mikuláš Ferjenčík však závěry analýzy odmítá. „Nemyslím si, že bychom šli cestou negativní kampaně. Ta kampaň je naopak pozitivní. Je to kampaň za vymahatelnost práva, kampaň za to, aby se korupční kauzy vyšetřovaly, kampaň za nezávislost justice. My se celou dobu snažíme vysvětlovat svůj program a i na tom autobusu je zadní strana, která se věnuje programu Pirátský strany do voleb,“ popsal.
 
Vůči negativní kampani se vymezuje i KSČM. „Nejde o to bojovat s uprchlíky, ale bránit prostor České republiky před určitým bezpečnostním rizikem, které mohou někteří uprchlíci, o kterých nic nevíme, sebou přinášet a samozřejmě je to i obrana vůči narušování suverenity Česka ze strany Bruselu,“ řekl poslanec a dvojka pražské kandidátky KSČM Jiří Dolejš.
 
Negativní je kampaň spolu s osobním životem politiků a prezentací programu jedním z častějších témat i u lidovců a ANO, celkově však komunikují spíše pozitivně. „Té negace od tradičních politických stran je ve veřejném prostoru až příliš moc. My jsme chtěli lidem v České republice pozitivní cestou představit náš program a se vší vážností jim popsat, jaké jsou priority hnutí ANO, pokud nám v těchto důležitých volbách, kde je o všechno, dají svůj hlas,“ vysvětlil mluvčí ANO Vladimír Vořechovský.

Podle vedoucího výzkumu Miloše Gregora komunikace hnutí ANO odráží způsob, jak většina uživatelů Facebook používá. „Málokdo z nás si chce na Facebooku číst analýzy nebo programová prohlášení. Hnutí se celkově snaží komunikovat spíše emočně, díky čemuž se částečně vyhýbá diskuzi s uživateli Facebooku o programových bodech a věcných argumentech,“ napsal serveru iROZHLAS.cz.

_Komunikační témata vybraných stran jsou shrnutá v následujícím grafu (kromě momentek z kontaktní kampaně, které tvoří asi polovinu celku)._

<wide>
<div id="temata" style="min-width: 300px; max-width: 1000px; height: 400px; margin: 0 auto"></div>
</wide>

KDU-ČSL se profiluje na otázkách rodiny, zemědělství a školství, příliš ale nemluví o ekonomice nebo povolebním vyjednávání, negativní kampani se také vyhýbá. Obecně opadl zájem o témata spojená s minulými lety, jako je korupce a energetika, partaje se pak, s výjimkou ČSSD a ANO, také příliš nevyjadřují k dopravě.

S blížícím se termínem voleb jsou stranické profily čím dál aktivnější. Týká se to zejména mladších stran, jako jsou Piráti, ANO nebo SPD; poměrně aktivní jsou i lidovci. Aktivita partají obvykle kulminuje v polovině týdne, přičemž zejména komunisti přispívají na FB v pravidelném rytmu.

<wide>
<div id="cas" style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto"></div>
</wide>

## Jak analýza vznikala

Výzkumníci pod vedením Miloše Gregora z <a target="_blank" href="https://www.facebook.com/politologiefss/ ">katedry politologie FSS Masarykovy univerzity</a> ohodnotili přes 7 tisíc příspěvků na celostátních a krajských FB profilech stran, šlo o příspěvky od 1. září a 13. října. Politologové se věnovali osmi partajím, které vytipovali na základě volebních průzkumů během července a srpna.

Kódování probíhalo podle upravené <a target="_blank" href="https://manifesto-project.wzb.eu/">metodiky CMP</a> a jednotlivý pracovníci prošli testem, který měl za úkol sjednotit vnímání a kódování proměnných.

„Metoda je užívána akademiky na tematické třídění volebních programů po celém světě, jedná se tedy o osvědčenou a spolehlivou metodu,“ napsal Gregor. Připustil ale, že určitou slabinou je rozhodování jednotlivce, který zařazování příspěvků provádí. „Některé statusy mohou být vícetematické. Kodér pak musí na základě svého nejlepšího uvážení rozhodnout, které téma podle něj ve statusu převažuje,“ vysvětlil.